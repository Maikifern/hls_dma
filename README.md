# hls_dma - a simple yet versatile HLS-implemented DMA engine 
This repository contains sources of a simple DMA engine with the core part written
in HLS. That enables easy customization for specific needs.
The design is described in the [paper](https://doi.org/10.3390/electronics12040883) "Versatile DMA Engine for High-Energy Physics Data Acquisition Implemented with High-Level Synthesis".

If you use it in your design, please cite https://doi.org/10.3390/electronics12040883 .

The Linux driver for the driver and the skeleton data-processing application is available
in the separate [gitlab repository](https://gitlab.com/WZab/wzdaq_drv).

Due to HLS limitations, the HLS part must be accompanied with certain blocks implemented in VHDL.

The test_env directory contains a testbench enabling testing in the simulation the HLS-generated RTL code. This part is significantly based on the code written by Dan Gisselquist and published in his [WB2AXIP](https://github.com/ZipCPU/wb2axip) repository.

All files (if not stated otherwise in the file itself) are licensed under the Dual GPL/BSD license.

