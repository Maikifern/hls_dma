-------------------------------------------------------------------------------
-- Title      : Simple reset generator
-- Project    : 
-------------------------------------------------------------------------------
-- File       : gen_reset.vhd
-- Author     : Wojciech M. Zabolotny <wzab01@gmail.com>
-- Company    : 
-- Created    : 2016-08-09
-- Last update: 2023-02-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This block generates the reset pulse of defined length
--              after the FPGA gets configured.
-------------------------------------------------------------------------------
-- Copyright (c) 2016
-- License    : Creative Commons Public Domain Dedication and Certification
-- SPDX       : CC-PDDC
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-08-09  1.0      xl      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------------------
entity gen_reset is

  port (
    -- System interface
    clk    : in  std_logic;
    resetn : out  std_logic
    );

end entity gen_reset;

architecture rtl of gen_reset is

  signal counter : integer := 200000000;
  signal s_resetn : std_logic := '0';
  
begin 

  resetn <= s_resetn;
  r1: process (clk) is
  begin  -- process r1
    if clk'event and clk = '1' then  -- rising clock edge
      if counter > 0 then
        counter <= counter - 1;
      else
        s_resetn <= '1';
      end if;      
    end if;
  end process r1;
  
end architecture rtl;
