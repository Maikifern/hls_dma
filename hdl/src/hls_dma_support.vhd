-------------------------------------------------------------------------------
-- Title      : Support logic for HLS-implemented DMA core
-- Project    : 
-------------------------------------------------------------------------------
-- File       : hls_dma_support.vhd
-- Author     : Wojciech M. Zabolotny <wzab01@gmail.com>,<wojciech.zabolotny@pw.edu.pl>
-- Company    : 
-- Created    : 2022-02-01
-- Last update: 2023-01-02
-- Platform   :
-- License    : Dual: GPL/BSD
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This file replaces the BD-implemented logic in HLS-DMA core
--              Development of this core is partially supported by the
--              European Union’s Horizon 2020 research and innovation programme
--              under grant agreement No 871072.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2022 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2022-02-01  1.0      WZab    Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------------------
entity hls_dma_support is

  port (
    -- Target buffer management 
    nr_pkt      : in  std_logic_vector(31 downto 0);
    nr_buf      : in  std_logic_vector(31 downto 0);
    srv_pkt     : in  std_logic_vector(31 downto 0);
    cur_pkt     : in  std_logic_vector(31 downto 0);
    cur_buf     : in  std_logic_vector(31 downto 0);
    overrun     : in  std_logic;
    -- HLS core control
    ap_start    : out std_logic;
    ap_done     : in  std_logic;
    ap_ready    : in  std_logic;
    ap_idle     : in  std_logic;
    ap_rst_n    : out std_logic;
    -- AXI registers connection 
    ctrl_out    : out std_logic_vector(31 downto 0);
    ctrl_in     : in  std_logic_vector(31 downto 0);
    -- Data source control
    src_start   : out std_logic;
    -- Interrupt control
    irq_request : out std_logic
    );

end entity hls_dma_support;

architecture rtl of hls_dma_support is


  signal pkt_av     : std_logic := '0';
  signal irq_enable : std_logic := '0';


begin  -- architecture rtl

  -- Connect bits to ctrl_out
  process (ap_done, ap_idle, ap_ready, overrun, pkt_av) is
  begin  -- process
    ctrl_out    <= (others => '0');
    ctrl_out(0) <= ap_done;
    ctrl_out(1) <= ap_ready;
    ctrl_out(2) <= ap_idle;
    ctrl_out(3) <= overrun;
    ctrl_out(4) <= pkt_av;
  end process;

  -- Extract bits from ctrl_in
  ap_start   <= ctrl_in(0);
  ap_rst_n   <= ctrl_in(1);
  src_start  <= ctrl_in(2);
  irq_enable <= ctrl_in(3);

  -- Check if there is a packet to be sent for processing
  pkt_av <= '1' when nr_pkt /= srv_pkt else '0';

  -- Check conditions for interrupt generation
  process (irq_enable, overrun, pkt_av) is
  begin  -- process
    irq_request <= '0';
    if irq_enable = '1' then
      if overrun = '1' then
        irq_request <= '1';
      end if;
      if pkt_av = '1' then
        irq_request <= '1';
      end if;
    end if;
  end process;

end architecture rtl;
