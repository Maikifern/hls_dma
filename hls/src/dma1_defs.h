// Created by Wojciech M. Zabolotny (wzab01<at>gmail.com or wojciech.zabolotny<at>pw.edu.pl)
// License: Dual GPL/BSD

#ifndef _dma1_defs_HH_
#define _dma1_defs_HH_

typedef ap_uint<256> AXI_VALUE;
typedef ap_uint<64> AXI_ADDR;
typedef ap_axiu<256, 1, 1, 1> AXIS_DATA;
typedef struct {
	AXI_VALUE dta;
} BUF_DATA;

typedef struct {
	ap_uint<32> count;
	ap_uint<32> word;
	ap_uint<1> eop;
	ap_uint<1> nextbuf;
} BURST_MARK;

typedef struct {
	ap_uint<64> base;
	ap_uint<64> first;
	ap_uint<64> after;
	ap_uint<32> count;
	ap_uint<32> packet;
	ap_uint<32> nr_buf;
	ap_uint<1> overrun;
	ap_uint<1> eop;
} OUTPUT_CHUNK;

typedef struct {
	ap_uint<32> nr_buf;
	ap_uint<32> nr_pkt;
	ap_uint<1> overrun;
	ap_uint<1> eop;
} OUTPUT_SIGS;

typedef struct {
	ap_uint<64> first;
	ap_uint<64> after;
	ap_uint<64> filler[2];
} PKT_DESC;

static const int BUFFER_FACTOR = 2;

static const int MAX_BURST_LENGTH = 2048;

// Buffer sizes
static const int DATA_DEPTH = MAX_BURST_LENGTH * BUFFER_FACTOR;
static const int COUNT_DEPTH = 2*BUFFER_FACTOR;

static const int CHUNKS_DEPTH = 2*BUFFER_FACTOR; // ?@?
//Maximal number of buffers
static const int NBUFS = 2048;
//How many packets can we have?
//We want them all to fit in a single huge page
#define NPKTS (2*1024*1024/32)
// We use 2MB huge pages as buffers - we keep 256-bits (32-bytes) values in them...
#define BUFLEN (2*1024*1024/32)

#endif
