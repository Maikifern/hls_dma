#include <cstdlib>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_uint<64> AXI_ADDR;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define NBUFS (8)
#define NPKTS (16)
#define BUFLEN (256/8)
const int TMPBUFLEN=256;

void dma1 (AXIS_DATA &din, volatile AXI_VALUE *a, AXI_ADDR bufs[NBUFS], ap_uint<1> work ){
#pragma HLS INTERFACE ap_none port=work
#pragma HLS INTERFACE axis port=din
#pragma HLS INTERFACE m_axi depth=TMPBUFLEN port=a offset=slave
#pragma HLS INTERFACE s_axilite port bufs

  ap_uint<64> aaddr;
  int i;
  int nr_buf, nr_word, nr_pkt;
  AXI_VALUE buff[TMPBUFLEN];

  //memcpy creates a burst access to memory
  //multiple calls of memcpy cannot be pipelined and will be scheduled sequentially
  //memcpy requires a local buffer to store the results of the memory transaction
  if(work == 0) {
    nr_buf = 0;
    nr_word = 0;
    nr_pkt = 0;
  } else { 
	   //Below didn't work when I tried to set the a pointer and then write to the referenced cell
	   // a = ...
	   // *a = ...
	   //It complained that can't cast from integer to pointer.
       aaddr=(bufs[nr_buf]+nr_word);
       a[aaddr] = din.data;
       if(din.last) {
          //We record the segment position in the another buffer
          aaddr= nr_pkt;
          a[aaddr]=nr_buf*BUFLEN+nr_word;
          nr_pkt++;
          if(nr_pkt == NPKTS) nr_pkt = 0;
       }
       nr_word++;
       if (nr_word==BUFLEN) {
          nr_word = 0;
          nr_buf++;
          if(nr_buf == NBUFS) {
             nr_buf = 0;
          }
    }
  }
}
