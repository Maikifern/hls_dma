#include <cstdlib>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define TMPBUFLEN (256)
#define BUFLEN (1024*1024*2/8)

void dma1(AXIS_DATA & din, ap_uint<1> ena, volatile AXI_VALUE * mout)
{
#pragma HLS_INTERFACE m_axi port=mout depth=TMPBFLEN  offset=slave bundle=gmem0
#pragma HLS_INTERFACE axis port = din
#pragma HLS_INTERFACE ap_none port = ena
	AXI_VALUE tmpbuf[TMPBUFLEN];
	int i=0;
	if(ena) {
		AXI_VALUE data;
		for(i=0;i<TMPBUFLEN;i++) {
			data = din.data;
			tmpbuf[i] = data;
			if(din.last)
				break;
		}
		memcpy((AXI_VALUE *)mout, (AXI_VALUE *)tmpbuf, TMPBUFLEN * sizeof(AXI_VALUE));
	}

}
